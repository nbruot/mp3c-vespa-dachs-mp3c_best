<resource schema="mp3c_best">
    <meta name="creationDate">2022-01-11T10:11:04Z</meta>

    <meta name="title">Minor Planet Physical Properties Catalogue - Best Values</meta>
    <meta name="shortName">MP3C-BEST</meta>
    <meta name="description">
        This service publishes average values of various physical properties for
        minor bodies. It mirrors part of the data reported by the Minor Planet
        Physical Properties Catalogue (MP3C) user-friendly interface at
        https://mp3c.oca.eu/. Quantities include masses, diameters, albedos and
        orbital parameters.
    </meta>
    <meta name="subject">solar-system-astronomy</meta>
    <meta name="subject">planetary-science</meta>
    <meta name="subject">asteroids</meta>
    <meta name="subject">small-solar-system-bodies</meta>
    <meta name="subject">astronomy-data-analysis</meta>
    <meta name="subject">astronomy-databases</meta>
    <meta name="subject">catalogs</meta>
    <meta name="subject">distance-measure</meta>
    <meta name="subject">dwarf-planets</meta>
    <meta name="subject">eccentricity</meta>
    <meta name="subject">inclination</meta>
    <meta name="subject">magnitude</meta>
    <meta name="subject">main-belt-asteroids</meta>
    <meta name="subject">major-axis</meta>
    <meta name="subject">natural-satellites-solar-system</meta>
    <meta name="subject">orbital-elements</meta>
    <meta name="subject">orbits</meta>

    <meta name="creator.name">Delbo, Marco</meta>

    <meta name="publisher.name">DOMINO</meta>

    <meta name="contributor.name">Delbo, Marco</meta>
    <meta name="contributor.name">Bruot, Nicolas</meta>
    <meta name="contributor.name">Avdellidou, Chrysa</meta>
    <meta name="contributor.name">Côte d'Azur Observatory</meta>
    <meta name="contributor.name">DOMINO</meta>

    <meta name="contact.name">MP3C</meta>
    <meta name="contact.email">mp3c@oca.eu</meta>
    <meta name="contact.address">
        Observatoire de la Côte d'Azur, Boulevard de l'Observatoire, CS 34229,
        06304 Nice Cedex 4, France
    </meta>
    <meta name="referenceURL">https://mp3c.oca.eu/</meta>

    <meta name="source">2013DPS....4520829T</meta>
    <meta name="type">Catalog</meta>

    <meta name="contentLevel">General</meta>
    <meta name="contentLevel">University</meta>
    <meta name="contentLevel">Research</meta>
    <meta name="contentLevel">Amateur</meta>


    <table id="epn_core" onDisk="true" adql="True">
        <mixin>
            <spatial_frame_type>none</spatial_frame_type>
            <optional_columns>alt_target_name mass diameter semi_major_axis inclination eccentricity magnitude albedo publisher producer_name</optional_columns>
            //epntap2#table-2_0
	    </mixin>

        <column>
            <name>parent</name>
            <type>integer</type>
            <values nullLiteral="-1"/>
            <description>
                Number of target parent (asteroid family number)
            </description>
            <ucd>meta.id.parent</ucd>
            <verbLevel>1</verbLevel>
        </column>

        <column>
            <name>parent_name</name>
            <type>text</type>
            <description>
                Name of target parent (asteroid family)
            </description>
            <ucd>meta.id.parent</ucd>
            <verbLevel>1</verbLevel>
        </column>

        <column>
            <name>mass_error</name>
            <type>real</type>
            <description>
                Mass of object error
            </description>
            <unit>kg</unit>
            <ucd>stat.error;phys.mass</ucd>
            <verbLevel>1</verbLevel>
        </column>

        <column>
            <name>diameter_error</name>
            <type>real</type>
            <description>
                Target diameter error
            </description>
            <unit>km</unit>
            <ucd>stat.error;phys.size.diameter</ucd>
            <verbLevel>1</verbLevel>
        </column>

        <column>
            <name>semi_major_axis_error</name>
            <type>real</type>
            <description>
                Orbital proper semimajor axis error
            </description>
            <unit>AU</unit>
            <ucd>stat.error;phys.size.smajAxis</ucd>
            <verbLevel>1</verbLevel>
        </column>

        <column>
            <name>inclination_error</name>
            <type>real</type>
            <description>
                Orbit proper inclination error
            </description>
            <unit>deg</unit>
            <ucd>stat.error;src.orbital.inclination</ucd>
            <verbLevel>1</verbLevel>
        </column>

        <column>
            <name>magnitude_error</name>
            <type>real</type>
            <description>
                Absolute magnitude error, from HG magnitude system
            </description>
            <ucd>stat.error;phys.magAbs</ucd>
            <unit>mag</unit>
            <verbLevel>1</verbLevel>
        </column>

        <column>
            <name>slope_parameter</name>
            <type>real</type>
            <description>
                Slope parameter (surge in brightness), observed when the body is near opposition
            </description>
            <verbLevel>1</verbLevel>
        </column>

        <column>
            <name>slope_parameter_error</name>
            <type>real</type>
            <description>
                Error of slope parameter (surge in brightness), observed when the body is near opposition
            </description>
            <verbLevel>1</verbLevel>
        </column>

        <column>
            <name>eccentricity_error</name>
            <type>real</type>
            <description>
                Orbit eccentricity
            </description>
            <ucd>stat.error;src.orbital.eccentricity</ucd>
            <verbLevel>1</verbLevel>
        </column>

        <column>
            <name>albedo_error</name>
            <type>real</type>
            <description>
                Target albedo error
            </description>
            <ucd>stat.error;phys.albedo</ucd>
            <verbLevel>1</verbLevel>
        </column>

        <column>
            <name>external_link</name>
            <type>text</type>
            <description>
                Link to a web page providing more details on the granule
            </description>
            <ucd>meta.ref.url</ucd>
            <verbLevel>2</verbLevel>
            <required>True</required>
        </column>
    </table>

    <data id="import">
        <sources>data/sample.csv</sources>

        <csvGrammar>
            <rowfilter procDef="//products#define">
                <bind name="table">"\schema.epn_core"</bind>
            </rowfilter>
        </csvGrammar>

        <make table="epn_core">
            <rowmaker idmaps="*">
                <map key="publisher">"DOMINO"</map>
                <map key="producer_name">"MP3C"</map>
                <map key="spatial_frame_type">"none"</map>
                <apply procDef="//epntap2#populate-2_0" name="fillepn">
                    <bind name="granule_uid">@target_name</bind>
                    <bind name="granule_gid">"mp3c"</bind>
                    <bind name="dataproduct_type">"ci"</bind>
                    <bind name="target_class">@target_class</bind>
                    <bind name="target_name">@target_name</bind>
                    <bind name="instrument_host_name">None</bind>
                    <bind name="obs_id">@target_name</bind>
                    <bind name="processing_level">5</bind>
                    <bind name="creation_date">@creation_date</bind>
                    <bind name="modification_date">@modification_date</bind>
                    <bind name="release_date">@release_date</bind>
                    <bind name="service_title">"mp3c_best"</bind>
                </apply>
            </rowmaker>
        </make>
    </data>
</resource>
